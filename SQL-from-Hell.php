<?php
class SQLfromHell
{
	private $conn;
	private $dbname;
	public $tables = [];

	public function __construct($dbname, $dbserver, $dbuser, $dbpassword)
	{
		$this->conn = mysqli_connect($dbserver, $dbuser, $dbpassword);
		if ($this->conn->connect_error) {
			die("Connection failed: " . $this->conn->connect_error);
		}
		$this->dbname = $dbname;
	}

	public function AddTable($name, $table)
	{
		$this->tables[$name] = $table;
	}

	public function MakeDB()
	{
		echo '<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1.0">' .
			'<title>SQL-from-Hell</title><link rel="stylesheet" href="https://pmke.de/normalize.css"></head><body><h1>SQL-from-Hell</h1>';

		if ($this->TestIfDatabaseExists()) {
			echo "<p>Status: Database '" . $this->dbname . "' exists</p>";
		} else {
			echo "<p>Status: Create Database:</p><pre>CREATE DATABASE `" . $this->dbname . "`;</pre>"; // IF NOT EXISTS
		}

		foreach ($this->tables as $table) {
			echo '<hr><div class="flex"><div><table class="er"><thead><tr><th colspan="4">' .
				$table->name . '</th></tr></thead><tbody>';
			foreach ($table->GetColumns() as $column) {
				echo '<tr><td>' . $column['column'] . '</td><td>' . $column['type'] .
					'</td><td>' . ($column['nullable'] ? 'true' : 'false') . '</td><td>' . '</td></tr>';
			}
			echo '</tbody><tfoot><tr><td>column</td><td>type</td><td>nullable</td><td></td></tr></tfoot></table></div>';

			echo '<div class="pl-2">';
			if ($this->TestIfTableExists($table->name)) {
				echo '<p>Status: Table exists</p><pre>SELECT * FROM `' . $this->dbname . '`.`' . $table->name . '`;</pre>';
			} else {
				echo '<p>Create Table:</p><pre>';
				echo 'CREATE TABLE `' . $this->dbname . '`.`' . $table->name . '` (<br>`';
				echo $table->primarykey . '` INT NOT NULL AUTO_INCREMENT PRIMARY KEY';
				foreach ($table->GetColumns() as $column) {
					if ($column['column'] != $table->primarykey) {
						echo ",<br>`" . $column['column'] . "` " . $column['type'];
						if (!$column['nullable']) {
							echo " NOT NULL";
						}
					}
				}
				foreach ($table->GetForeignkeys() as $foreignkey) {
					echo ",<br>" . "FOREIGN KEY (`" . $foreignkey['key'] . "`) REFERENCES "
						. $foreignkey['table'] . "(`" . $foreignkey['key'] . "`)";
					if ($foreignkey['cascade']) {
						echo " ON DELETE CASCADE";
					}
				}
				echo ");</pre>";
			}
			// new section planned: 'changes can be applied' -> alter table
			foreach ($table->GetConstraints() as $constraint) {
				if ($this->TestIfConstraintExists($table->name, $constraint['name'])) {
					echo '<p>Constraint <code>`' . $constraint['name'] . '`</code> set</p>';
				} else {
					echo '<p>Add Constraint:</p><pre>Alter Table `' . $this->dbname . "`.`" . $table->name . '` ADD CONSTRAINT '
						. $constraint['name'] . ' ' . $constraint['constraint'] . ';</pre>';
				}
			}
			echo '</div>';
			echo '</div>';
		}
		CSS();
		echo '</body></html>';
	}

	private function TestIfDatabaseExists(): bool
	{
		$res = mysqli_query($this->conn, "SELECT count(*) as count FROM information_schema.schemata WHERE schema_name = '" .
			$this->dbname . "';");
		$data = mysqli_fetch_assoc($res);
		return $data["count"] == 1;
	}

	private function TestIfTableExists($table): bool
	{
		$res = mysqli_query($this->conn, "SELECT count(*) as count FROM information_schema.tables WHERE table_schema = '" .
			$this->dbname . "' AND table_name = '" . $table . "';");
		$data = mysqli_fetch_assoc($res);
		return $data["count"] == 1;
	}

	private function TestIfConstraintExists($table, $constraint): bool
	{
		$res = mysqli_query($this->conn, "SELECT count(*) as count FROM information_schema.table_constraints WHERE table_name = '" .
			$table . "' AND constraint_name = '" . $constraint . "';");
		$data = mysqli_fetch_assoc($res);
		return $data["count"] == 1;
	}

	public function __destruct()
	{
		$this->conn->close();
	}
}

class Table
{
	public $name;
	public $primarykey;
	private $foreignkeys = array();
	private $columns = array();
	private $constraints = array();

	public function __construct($tablename, $primarykey)
	{
		$this->name = $tablename;
		$this->primarykey = $primarykey;
		$this->AddColumn($primarykey, "INT", false);
	}

	public function AddColumn($column, $type, $nullable = false)
	{
		array_push(
			$this->columns,
			array("column" => $column, "type" => $type, "nullable" => $nullable)
		);
	}

	public function AddForeignKey($key, $nullable, $cascade)
	{
		$this->AddColumn($key["key"], "INT", $nullable);
		array_push(
			$this->foreignkeys,
			array(
				"key" => $key["key"],
				"table" => $key["table"],
				"cascade" => $cascade
			)
		);
	}

	public function AddConstraint($name, $constraint)
	{
		array_push(
			$this->constraints,
			array(
				"name" => $name,
				"constraint" => $constraint
			)
		);
	}

	public function GetPrimaryKey(): array
	{
		return array('key' => $this->primarykey, 'table' => $this->name);
	}

	public function GetColumns(): array
	{
		$r = $this->columns;
		array_push($r, array("column" => "CreateUser", "type" => "INT", "nullable" => false));
		array_push($r, array("column" => "UpdateUser", "type" => "INT", "nullable" => false));
		array_push($r, array("column" => "Created", "type" => "DATETIME DEFAULT current_timestamp()", "nullable" => false));
		array_push($r, array("column" => "Updated", "type" => "DATETIME DEFAULT current_timestamp()", "nullable" => false));
		return $r;
	}

	public function GetForeignkeys(): array
	{
		return $this->foreignkeys;
	}

	public function GetConstraints(): array
	{
		return $this->constraints;
	}
}

function CSS()
{
?>


	<style>
		html {
			margin: 2em;
			background: #f6f9fc;
		}

		body {
			padding: 2em;
			background: #ffffff;
			border: 1px solid #cccccc;
		}

		pre {
			background-color: #ebf1f5;
			border: 1px solid #bbccdd;
			border-radius: 5px;
			line-height: 1.3;
			padding: 1em;
			color: #222222;
			white-space: pre-wrap !important;
			margin-bottom: 1.5em;
		}

		h1,
		p,
		pre {
			margin-top: 0;
		}

		hr {
			margin: 1em 0;
			border: 0;
			border-top: 1px solid #cccccc;
		}

		.flex {
			display: flex;
		}

		.er {
			border-collapse: collapse;
			border: 1px solid #282A35;
			min-width: 400px;
		}

		.er th,
		.er tfoot td {
			border-top: 1px solid #282A35;
			border-bottom: 1px solid #282A35;
			background-color: gainsboro;
			padding: 5px;
		}

		.er td {
			text-align: left;
			padding: 5px;
		}

		.pl-2 {
			padding-left: 2em;
		}
	</style>
<?php
}

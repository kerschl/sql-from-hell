<a name="readme-top"></a>
<br />

<div align="center">
	<!--a href="https://codeberg.org/peddamax/SQL-from-Hell">
	<img src="logo.png" alt="Logo" width="80" height="80">
	</a-->
	<h3 align="center">SQL-from-Hell</h3>
	<p align="center">
		Ein Programm, das grundlegende Designfails und über kreative Datentypennutzung verhindern soll. Setzt grundlegende Datenbankkenntnisse voraus.
	</p>
	<p align="center">
		Inspiriert von dem gleichnamigen <a href="https://www.youtube.com/watch?v=LR0dn_Qgmcg">Talk</a> auf der GPN18
	</p>
</div>

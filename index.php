<?php
include "./SQL-from-Hell.php";
include "./config.php";
$varchar = "VARCHAR(255)";
$datetime = "DATETIME";
$int = "INT";

/////////////////////////
// scheme defined here // 
/////////////////////////

$db = new SQLfromHell("pmke", $dbserver, $dbuser, $dbpassword); // new database Test // auth

$db->AddTable('Person', new Table("Person", "PersonID")); // create table Person with primary key PersonID
$db->tables['Person']->AddColumn("FirstName", $varchar, true); // add column "FirstName" with field type VARCHAR(255) and nullable = true
$db->tables['Person']->AddColumn("LastName", $varchar, false); // add column "LastName" with field type VARCHAR(255) and nullable = false

$db->AddTable('App', new Table("App", "AppID"));
$db->tables['App']->AddColumn("AppName", $varchar, false);
$db->tables['App']->AddConstraint("UniqueApps", "UNIQUE(`AppName`)"); // add constraint 'UniqueApp' to avoide duplicates

$db->AddTable('Users', new Table("Users", "UserID"));
$db->tables['Users']->AddForeignKey($db->tables['Person']->GetPrimaryKey(), false, true); // add foreign key and cascade on delete = true
$db->tables['Users']->AddColumn("Password", $varchar, true);

$db->MakeDB();

////////////////
// end scheme //
////////////////
